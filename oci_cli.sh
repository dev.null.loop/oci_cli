#!/bin/bash
# define regions
regions=("eu-amsterdam-1" "eu-paris-1" "uk-cardiff-1" "me-dubai-1" "eu-frankfurt-1" "sa-saopaulo-1" "us-ashburn-1" "me-jeddah-1" "uk-london-1" "eu-milan-1" "ap-melbourne-1" "eu-marseille-1" "us-phoenix-1" ap-singapore-1 us-sanjose-1 ap-sydney-1 eu-zurich-1)

# action using a different profile set in ~/.oci/config
oci iam region list --profile customer1

# processing json with jq: https://gist.github.com/olih/f7437fb6962fb3ee9fe95bda8d2c8fa4

# select fields using jq
curl -s 'https://launchermeta.mojang.com/mc/game/version_manifest.json' | jq '.versions[] | select(.type == "release") | .id'

# oci resource types: https://docs.oracle.com/en-us/iaas/Content/Search/Concepts/queryoverview.htm#resourcetypes
# list resource types
oci search resource-type list --all|jq -r '.data[].name'

# list compartments names recursively starting from tenancy
oci iam compartment list --all --compartment-id-in-subtree true|jq -r '.data[].name'

# list all resources in AVAILABLE lifecycleState
oci search resource structured-search --query-text "QUERY all resources where lifeCycleState != 'TERMINATED' && lifeCycleState != 'FAILED'" | jq -r '[.data.items[] | {displayName:."display-name" , id: .identifier ,lifecycleState: ."lifecycle-state"}]'

# list all drg from a region
oci search resource structured-search --query-text "query drg resources where lifeCycleState != 'TERMINATED' && lifeCycleState != 'FAILED'" |jq -r '.data.items[]."display-name"'

echo "list drg resources created before 2022-01-01..."
for i in ${regions[@]}; do oci search resource structured-search --limit 1000 --query-text "query drg resources where lifeCycleState != 'DELETED' && timeCreated < '2022-01-01T01:01:01Z'" --region $i; done

oci search resource structured-search \
  --query-text "query all resources where (definedTags.namespace = '$1' \
  && definedTags.key = '$2' \
  && definedTags.value = '$3')" \
  --output table \
  --query "data.items[*] | \
  sort_by(@,&\"resource-type\") \
  [].{Name:\"display-name\",Type:\"resource-type\",ID:identifier,AD:\"availability-domain\"}"

# list defined tags for a certain resource
oci network vcn get --vcn-id ocid1.vcn.oc1.eu-frankfurt-1.amaaaaaaywndk3aas5aokdclahlkkhdapdik5wkrlnjv2pmqzvfl7endnxza|jq -r '.data."defined-tags"'

# list resources created by a certain user
oci search resource structured-search --query-text "query all resources where definedTags.CreatedBy = oracleidentitycloudservice/decebal.popescu@oracle.com"

oci search resource structured-search \
  --query-text "query all resources where (    \
                   freeformTags.key   = '$1'   \
                && freeformTags.value = '$2')" \
  --output table \
  --query "data.items[*] | \
  sort_by(@,&\"resource-type\") \
  [].{Name:\"display-name\",Type:\"resource-type\",ID:identifier,AD:\"availability-domain\",State:\"lifecycle-state\"}"

# list drg resources in home region; add --region REGION parameter to change the region
oci search resource structured-search --query-text "query drg resources" |jq -r '.data.items[]."display-name"'
oci search resource structured-search --query-text "query drg resources" --region me-jeddah-1 | jq -r '.data.items[]."display-name"'

# list drgs in compartment - does not iterate over subcompartments
oci network drg list --compartment-id $bog|jq -r '.data[]."display-name"'

# list costs
oci usage-api  usage-summary  request-summarized-usages  --tenant-id $tenancy --time-usage-started 2022-05-01 --time-usage-ended 2022-05-02 --granularity MONTHLY

# list all available resources in home region
oci search resource structured-search --query-text "QUERY all resources where lifeCycleState != 'TERMINATED' && lifeCycleState != 'FAILED'" | jq -r '[.data.items[] | {displayName:."display-name" , id: .identifier ,lifecycleState: ."lifecycle-state"}]'

# list all services
oci limits service list --compartment-id $tenancy|jq -r '.data[].name'

# includes a full list of resource limits belonging to a given service.
oci limits value list --compartment-id $tenancy --service-name vcn|jq -r '.data[].name'

# list all resources created by a certain user
oci search resource structured-search  --query-text "query all resources where definedTags.value = 'oracleidentitycloudservice/decebal.popescu@oracle.com'"|jq -r '.data.items[]'

# list publicIp resources created by a user
for i in ${regions[@]}; do oci search resource structured-search --limit 1000 --query-text "query publicIp resources where definedTags.value = 'oracleidentitycloudservice/decebal.popescu@oracle.com'" --region $i|jq -r '.data.items[]'; done

echo "list active publicIp resources in all regions..."
for i in ${regions[@]}; do oci search resource structured-search --limit 1000 --query-text "query publicIp resources where lifeCycleState != 'DELETED'" | jq -r '.data.items[]."defined-tags"."Oracle-Tags".CreatedBy'; done

# select based on regex jq (test)
oci ce node-pool-options get --node-pool-option-id all|jq -r '.data.sources[] | select (."source-name"| test("Oracle-Linux.*OKE.*"))'

echo "list all active resources in compartment X in home region"
oci search resource structured-search --query-text "query all resources where compartmentId='ocid1.compartment.oc1..aaaaaaaalsireogprnjmh7yxdrylqey66tm3sjnyf7vicnjtlm47tx4k7vuq'"|jq -r '.data.items[]|select(."lifecycle-state" != "DELETED")'

echo "list subcompartments in DELETED state..."
oci iam compartment list --compartment-id $bog|jq -r '.data[] | select(."lifecycle-state" == "DELETED") | .id'

# for i in `oci iam compartment list --compartment-id $bog|jq -r ' .data[] | select(."lifecycle-state" != "DELETED")'|jq -r '.id'`; do
#   oci search resource structured-search --query-text "query all resources where compartmentId='$i'" --region me-jeddah-1|jq -r '.data.items[].identifier';
# done

echo "list all limits values for all services and display limits using regexp..."
for i in `oci limits service list --compartment-id $tenancy | jq -r '.data[].name'`; do oci limits value list --all --compartment-id $tenancy --service-name $i|jq -r '.data[]|select(.name| test(".*public.*"))'; done

echo "list vaults in home region, in PENDING_DELETION state..."
oci search resource structured-search --query-text "query vault resources" |jq -r '.data.items[]|select(."lifecycle-state" == "PENDING_DELETION")' | jq -r '."display-name"'

echo "list docker image in compartment..."
comp=ocid1.compartment.oc1..aaaaaaaaxzepkap5upigqeg2pg2unobc5kj5uek3ya7sxfnahrhm2nlzu2va
oci artifacts container image list --compartment-id $comp --profile customer1 --region us-sanjose-1|jq -r '.data.items[]."display-name"'
oci artifacts container image list --compartment-id $customer1 --profile customer1 --region us-sanjose-1|jq -r '.data.items[]."display-name"'
oci artifacts repository  list --compartment-id $comp --profile customer1 --all

echo "list artifacts from repository..."
repo=ocid1.artifactrepository.oc1.us-sanjose-1.0.amaaaaaaqmdr6taae3wcz6nfitqggcx7rwsmhsi7t24tm5o7cpyhskk2s26a
oci artifacts generic artifact  list --compartment-id $comp --repository-id $repo --profile customer1 --all|jq -r '.data.items[]."display-name"'
