#!/usr/bin/bash
# The good, the bad, and the ugly http://www.bolthole.com/solaris/ksh-sampleprog.html
# Processing JSON using jq: https://gist.github.com/olih/f7437fb6962fb3ee9fe95bda8d2c8fa4
# NOTE: vcns are REGIONAL. Check your region before running the script
# order of deletion from a vcn:
# delete all subnets
# delete all route tables but default
# update default route table route-rules to []
# delete all gateways (drg, igw, lpg, sgw ngw)
# delete all security lists but default
# update default security list rules to []
# delete vcn

usage() {
        printf "Usage: vcn_delete.sh \${vcn_id}\n"
}

if [ ! -x /usr/bin/jq ] ; then
    printf "ERROR: jq not installed on this machine. Cannot continue."
    usage
    exit 1
fi

if [ $# -ne 1 ] ; then
    printf "ERROR: no vcn_id specified\n"
    usage
    exit 1
fi

vcn_id=$1
# get all subnets
subnets=$(oci network subnet list --compartment-id $bog|jq -r ".data[] |select (.\"vcn-id\" == \"${vcn_id}\")|.id")
for i in $subnets; do
    oci network subnet delete --force --subnet-id $i
done

# get all route table ids but default
route_tables=$(oci network route-table  list --compartment-id $bog | jq -r ".data[] | select (.\"vcn-id\" == \"${vcn_id}\" and (.\"display-name\" |test(\"Default*\")|not))|.id")
# delete all route tables but default
for i in ${route_tables}; do
    oci network route-table delete --force --rt-id $i;
done
# get default route table id
default_route_table=$(oci network route-table  list --compartment-id $bog | jq -r ".data[] | select (.\"vcn-id\" == \"${vcn_id}\" and (.\"display-name\" |test(\"Default*\")))|.id")
# delete all route rules from default route table
oci network route-table update --force --rt-id ${default_route_table} --route-rules "[]"

# delete drg, ig, lpg, ngw, sgw
declare -A ids=( ["drg"]="--drg-id" ["internet-gateway"]="--ig-id" ["local-peering-gateway"]="--local-peering-gateway" ["nat-gateway"]="--nat-gateway-id" ["service-gateway"]="--service-gateway-id")
for i in ${!ids[@]}; do
    id=$(oci network $i list --compartment-id $bog|jq -r ".data[] |select (.\"vcn-id\" == \"${vcn_id}\") | .id")
    if [ ! -z $id ]; then
        oci network $i delete --force ${ids["$i"]} $id
    fi
done

# get all security lists but default
security_lists=$(oci network security-list list --compartment-id $bog | jq -r ".data[] | select (.\"vcn-id\" == \"${vcn_id}\" and (.\"display-name\" | test(\"Default*\")|not)) | .id")
for i in $security_lists; do
    oci network security-list delete --force --security-list-id $i
done
# get the DEFAULT security list id and remove all rules
default_security_list=$(oci network security-list list --compartment-id $bog | jq -r ".data[] |select (.\"vcn-id\" == \"${vcn_id}\") | .id")
# delete all (in|e)gress rules from default security list
oci network security-list update --force --security-list-id $default_security_list --ingress-security-rules '[]' --egress-security-rules '[]'

# delete vcn
oci network vcn delete --force --vcn-id ${vcn_id}
